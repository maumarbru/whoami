FROM python:3.6

RUN pip3 install jupyter

WORKDIR /usr/src/app

ENV PATH /usr/src/app/bin:$PATH

ENV PYTHONPATH /usr/src/app:$PYTHONPATH 

CMD jupyter-default
