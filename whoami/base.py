import time
import asyncio

PERIODO_INCONSIENTE_SEGUNDOS = 1


def dondeEsta(ser):
    return id(ser)


def quienEs(ser):
    pass  # resultado difuso de los recuerdos


def tiempo():
    return time.monotonic()


class Ser:

    def __init__(self, name=None, cantidad_de_recuerdos=10):
        self.name = name
        self.recuerdos = Recuerdos(cantidad_de_recuerdos)
        self.nacimiento = tiempo()
        self.vida = asyncio.ensure_future(self.vivir())

    @property
    def dondeEstoy(self):
        return dondeEsta(self)

    @property
    def quienSoy(self):
        return quienEs(self)

    async def vivir(self):
        while True:
            await asyncio.sleep(PERIODO_INCONSIENTE_SEGUNDO)
            if self.durmiendo:
                continue
            self.recuerdos.recordar(self)

    def dormir(self):
        self.durmiendo = True

    def despertar(self):
        self.durmiendo = False

    def morir(self):
        self.vida.cancel()

    def __repr__(self):
        return f'{type(self).__name__}: {self.name}'


class Recuerdos(dict):

    def __init__(self, cantidad=10):
        self.cantidad = cantidad

    def recordar(self, ser):
        if len(self) >= self.cantidad:
            olvidar_instante = random.choice(list(self.keys()))
            del self[olvidar_instante]
        self[tiempo()] = ser

    @property
    def ultimo(self):
        return self[-1]
