La idea
-------

Hacer un sistema de autoreconocimiento medio tirado de los pelos.

La propuesta es trabajar con Jupyter/IPython notebook.


Instalación
-----------
```
git clone git@gitlab.com:maumarbru/whoami.git
cd whoami
docker build -t whoami .
```
Esto va a usar la receta que está en el `Dockerfile` en la carpeta actual


Ejecución
---------
Se va a ejecutar por defecto Jupyter notebook que es lo que propongo que usemos para editar los archivos y correr el notebook `whoami.ipynb`

Desde dentro de la carpeta del repo, ejecutar:
```
docker run --rm -v $PWD:/usr/src/app -p 8888:8888 -it whoami
# o sino
bin/docker-run
```
- `--rm` para que el container se elimine luego de la ejecución
- `-v $PWD:/usr/src/app` monta la carpeta actual, la del repo, en modo lectura/escritura
- `-p 8888:8888` expone el puerto 8888 del container a el puerto 8888 del host, para poder acceder a jupyter notebook desde el host.